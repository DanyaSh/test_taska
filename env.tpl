#Telegram bot token
TOKEN_BOT=your_token

#Tokens for your openWeather and Exchange
TOKEN_WEATHER=yor_token
TOKEN_EXCHANGE=your_token

#Telegram chat id
GROUP_ID=your_id_for_chat

#invite link for telegram chat
LINK_WELCOME=https://invite_chat_link

#List id of admins for notifications
LIST_ADMIN=your_ids_with_,_split

#WEBHOOK
WEBHOOK_SECRET=any_string
WEBHOOK_PATH=/path_location
WEB_SERVER_HOST=0.0.0.0
WEB_SERVER_PORT=8000
BASE_WEBHOOK_URL=https://domen.com

#NGINX
DOMEN=domen.com
PROXY_PASS=http://service_name:8000

#SSL
SSL_FOLDER=/etc/SSL_FOLDER
SSL_KEY=/etc/SSL_FOLDER/PRIVATE_KEY.pem
SSL_CERT=/etc/SSL_FOLDER/CERT.pem
