start    =  "👋 Hi {name}!\n"
start   +=  "What do you want❓"

helps = "This is a simple bot for presentation and test devops skills."

city  = "Okay {name}, send me your geolocation or city name (Eng).\n"
city += "For example: Yekaterinburg"
    
choice_city  = "Select the city according to your request:\n"
choice_city += "🔍'{text}'"

exchange  = "Okay {name}, send me the pair or push the button."
exchange += "For example: USD-PLN"

exchange_input  = "Okay {name}, send me the value in {first} "
exchange_input += "for conversion to {second}"

exchange_output = "Now {value} {first} = {answer} {second}."

poll  = "Push the button to create poll.\n"
poll += "This poll will be posted in [our private chat]({link})."

cancel = '💤 Действие отменено'

sticker = 'This is a sticker!'

animation = 'This is a GIF!'

simple_text = 'This is a simple text!'